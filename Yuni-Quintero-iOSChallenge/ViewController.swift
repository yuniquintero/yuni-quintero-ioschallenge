//
//  ViewController.swift
//  Yuni-Quintero-iOSChallenge
//
//  Created by Yuni Quintero on 2/2/21.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    var usernameField = UITextField()
    var passwordField = UITextField()
    var imgMargin: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red:0.04, green:0.08, blue:0.11, alpha:1.0)
        
        let bgImg = UIImageView(image: UIImage(named: "mrrobot"))
        bgImg.contentMode = .scaleAspectFill
        bgImg.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bgImg)
        
        NSLayoutConstraint.activate([
            bgImg.topAnchor.constraint(equalTo: view.topAnchor),
            bgImg.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bgImg.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        let logo = UIImageView(image: UIImage(named: "logo"))
        logo.contentMode = .scaleAspectFit
        logo.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logo)
        
        NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
        
        imgMargin = logo.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -80)
        imgMargin.isActive = true
        
        usernameField.backgroundColor = UIColor.white
        usernameField.font = UIFont(name: "SFProText-Bold", size: 12)
        usernameField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)])
        usernameField.layer.cornerRadius = 5
        usernameField.textColor = UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)
        usernameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: usernameField.frame.height))
        usernameField.leftViewMode = .always
        usernameField.addTarget(self, action: #selector(doneTapped), for: .editingDidEndOnExit)
        
        usernameField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(usernameField)
        
        NSLayoutConstraint.activate([
            usernameField.heightAnchor.constraint(equalToConstant: 52),
            usernameField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80),
            usernameField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80),
            usernameField.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 50)
        ])
        
        passwordField.backgroundColor = UIColor.white
        passwordField.font = UIFont(name: "SFProText-Bold", size: 12)
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)])
        passwordField.layer.cornerRadius = 5
        passwordField.textColor = UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)
        passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: passwordField.frame.height))
        passwordField.leftViewMode = .always
        passwordField.addTarget(self, action: #selector(doneTapped), for: .editingDidEndOnExit)
        passwordField.isSecureTextEntry = true
        passwordField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(passwordField)
        
        NSLayoutConstraint.activate([
            passwordField.heightAnchor.constraint(equalToConstant: 52),
            passwordField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80),
            passwordField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80),
            passwordField.topAnchor.constraint(equalTo: usernameField.bottomAnchor, constant: 20)
        ])
        
        let submitBtn = UIButton()
        submitBtn.setTitle("Log in", for: .normal)
        submitBtn.setTitleColor(.white, for: .normal)
        submitBtn.titleLabel?.font = UIFont(name: "SFProText-Bold", size: 14)
        submitBtn.backgroundColor = UIColor(red: 0.13, green: 0.80, blue: 0.40, alpha: 1.0)
        submitBtn.layer.cornerRadius = 5
        submitBtn.addTarget(self, action: #selector(submitTapped), for: .touchDown)
        submitBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(submitBtn)
        
        NSLayoutConstraint.activate([
            submitBtn.heightAnchor.constraint(equalToConstant: 52),
            submitBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80),
            submitBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80),
            submitBtn.topAnchor.constraint(equalTo: passwordField.bottomAnchor, constant: 20)
        ])
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name:  UIResponder.keyboardWillChangeFrameNotification, object: nil)
        navigationController?.isNavigationBarHidden = true
        
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            imgMargin.constant = -80
            view.layoutIfNeeded()
        } else {
            imgMargin.constant = -250
            view.layoutIfNeeded()
        }
    }
    
    @objc func doneTapped() {
        view.endEditing(true)
    }
    
    @objc func submitTapped() {
        let urlString = apiURL + "/authentication/token/new" + apiKey
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default).responseJSON { myresponse in
                switch myresponse.result {
                case .success(let json):
                    let jsonData = json
                    if let dictionary = jsonData as? [String: Any] {
                        UserDefaults.standard.set(dictionary["request_token"], forKey: "token")
                        
                        guard let url = URL(string: "https://www.themoviedb.org/authenticate/" + (dictionary["request_token"] as! String)) else { return }
                        UIApplication.shared.open(url)
                        print(UserDefaults.standard.string(forKey: "token"))
                        let homeView = HomeViewController()
                        self.navigationController?.pushViewController(homeView, animated: true)
                    }
                case .failure( _):
                    let dialogMessage = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: .alert)
                    self.present(dialogMessage, animated: true, completion: nil)
                }
        }
        
    }
    
}

