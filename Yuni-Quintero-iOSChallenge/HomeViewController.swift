//
//  HomeViewController.swift
//  Yuni-Quintero-iOSChallenge
//
//  Created by Yuni Quintero on 2/3/21.
//

import UIKit
import Alamofire

struct TVShow {
    var posterPath: String
    var name: String
    var date: String
    var vote: String
    var summary: String
}

class HomeViewController: UIViewController {
    
    var popularBtn = UIButton()
    var ratedBtn = UIButton()
    var ontvBtn = UIButton()
    var airBtn = UIButton()
    
    var isPop = true
    var isTop = false
    var isOn = false
    var isAir = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red:0.04, green:0.08, blue:0.11, alpha:1.0)
        
        setupview()
        
        getPop()
    }
    
    @objc func getPop() {
        let urlString = apiURL + "/tv/popular" + apiKey
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default).responseJSON { myresponse in
                print(myresponse)
                switch myresponse.result {
                case .success(let json):
                    let jsonData = json
                    if let dictionary = jsonData as? [String: Any] {
                        
                    }
                case .failure( _):
                    let dialogMessage = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: .alert)
                    self.present(dialogMessage, animated: true, completion: nil)
                }
        }
    }
    
    @objc func menuTapped() {
        let alert = UIAlertController(title: "What do you want to do?", message: nil, preferredStyle: .actionSheet)
        let profileBtn = UIAlertAction(title: "View Profile", style: .default, handler: { _ in
            let profileView = ProfileViewController()
            profileView.modalPresentationStyle = .pageSheet
            self.present(profileView, animated: true, completion: nil)
        })
        alert.addAction(profileBtn)
        
        let logoutBtn = UIAlertAction(title: "Log out", style: .destructive, handler: { _ in
            print("LOGOUT")
        })
        alert.addAction(logoutBtn)
        
        let cancelBtn = UIAlertAction(title: "Cancel", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(cancelBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func popularTapped() {
        if !isPop {
            isPop = true
            popularBtn.backgroundColor = UIColor(red:0.39, green:0.39, blue:0.40, alpha:1.0)
            popularBtn.titleLabel?.font = UIFont(name: "SFProText-Semibold", size: 13)
            
            if isTop {
                isTop = false
                ratedBtn.backgroundColor = .clear
                ratedBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isOn {
                isOn = false
                ontvBtn.backgroundColor = .clear
                ontvBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isAir {
                isAir = false
                airBtn.backgroundColor = .clear
                airBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            }
        }
    }
    
    @objc func topTapped() {
        if !isTop {
            isTop = true
            ratedBtn.backgroundColor = UIColor(red:0.39, green:0.39, blue:0.40, alpha:1.0)
            ratedBtn.titleLabel?.font = UIFont(name: "SFProText-Semibold", size: 13)
            
            if isPop {
                isPop = false
                popularBtn.backgroundColor = .clear
                popularBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isOn {
                isOn = false
                ontvBtn.backgroundColor = .clear
                ontvBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isAir {
                isAir = false
                airBtn.backgroundColor = .clear
                airBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            }
        }
    }
    
    @objc func airTapped() {
        if !isAir {
            isAir = true
            airBtn.backgroundColor = UIColor(red:0.39, green:0.39, blue:0.40, alpha:1.0)
            airBtn.titleLabel?.font = UIFont(name: "SFProText-Semibold", size: 13)
            
            if isTop {
                isTop = false
                ratedBtn.backgroundColor = .clear
                ratedBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isOn {
                isOn = false
                ontvBtn.backgroundColor = .clear
                ontvBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isPop {
                isPop = false
                popularBtn.backgroundColor = .clear
                popularBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            }
        }
    }
    
    @objc func tvTapped() {
        if !isOn {
            isOn = true
            ontvBtn.backgroundColor = UIColor(red:0.39, green:0.39, blue:0.40, alpha:1.0)
            ontvBtn.titleLabel?.font = UIFont(name: "SFProText-Semibold", size: 13)
            
            if isTop {
                isTop = false
                ratedBtn.backgroundColor = .clear
                ratedBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isPop {
                isPop = false
                popularBtn.backgroundColor = .clear
                popularBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            } else if isAir {
                isAir = false
                airBtn.backgroundColor = .clear
                airBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
            }
        }
    }
    
    func setupview() {
        let banner = UIView()
        banner.backgroundColor = UIColor(red:0.12, green:0.16, blue:0.18, alpha: 0.85)
        banner.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(banner)
        
        NSLayoutConstraint.activate([
            banner.topAnchor.constraint(equalTo: view.topAnchor),
            banner.heightAnchor.constraint(equalToConstant: 88),
            banner.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            banner.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        let titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.font = UIFont(name: "SFProText-Semibold.ttf", size: 17)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "TV Shows"
        view.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: banner.centerYAnchor, constant: 10)
        ])
        
        let menuIcon = UIButton()
        menuIcon.setImage(UIImage(named: "menuIcon"), for: .normal)
        menuIcon.addTarget(self, action: #selector(menuTapped), for: .touchDown)
        menuIcon.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(menuIcon)
        
        NSLayoutConstraint.activate([
            menuIcon.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            menuIcon.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor)
        ])
        
        let filterBg = UIView()
        filterBg.backgroundColor = UIColor(red:0.46, green:0.46, blue:0.50, alpha:0.12)
        filterBg.translatesAutoresizingMaskIntoConstraints = false
        filterBg.layer.cornerRadius = 8
        view.addSubview(filterBg)
        
        NSLayoutConstraint.activate([
            filterBg.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 23),
            filterBg.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -23),
            filterBg.topAnchor.constraint(equalTo: banner.bottomAnchor, constant: 23),
            filterBg.heightAnchor.constraint(equalToConstant: 28)
        ])
        
        let screenWidth = UIScreen.main.bounds.width
        
        popularBtn.setTitle("Popular", for: .normal)
        popularBtn.titleLabel?.font = UIFont(name: "SFProText-Semibold", size: 13)
        popularBtn.setTitleColor(.white, for: .normal)
        popularBtn.backgroundColor = UIColor(red:0.39, green:0.39, blue:0.40, alpha:1.0)
        popularBtn.translatesAutoresizingMaskIntoConstraints = false
        popularBtn.layer.cornerRadius = 8
        popularBtn.addTarget(self, action: #selector(popularTapped), for: .touchDown)
        view.addSubview(popularBtn)
        
        NSLayoutConstraint.activate([
            popularBtn.leadingAnchor.constraint(equalTo: filterBg.leadingAnchor),
            popularBtn.centerYAnchor.constraint(equalTo: filterBg.centerYAnchor),
            popularBtn.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: ((-screenWidth) / 4) + 12)
        ])
        
        ratedBtn.setTitle("Top Rated", for: .normal)
        ratedBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
        ratedBtn.setTitleColor(.white, for: .normal)
        ratedBtn.translatesAutoresizingMaskIntoConstraints = false
        ratedBtn.layer.cornerRadius = 8
        ratedBtn.addTarget(self, action: #selector(topTapped), for: .touchDown)
        view.addSubview(ratedBtn)
        
        NSLayoutConstraint.activate([
            ratedBtn.leadingAnchor.constraint(equalTo: popularBtn.trailingAnchor),
            ratedBtn.centerYAnchor.constraint(equalTo: filterBg.centerYAnchor),
            ratedBtn.trailingAnchor.constraint(equalTo: view.centerXAnchor),
        ])
        
        ontvBtn.setTitle("On TV", for: .normal)
        ontvBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
        ontvBtn.setTitleColor(.white, for: .normal)
        ontvBtn.translatesAutoresizingMaskIntoConstraints = false
        ontvBtn.layer.cornerRadius = 8
        ontvBtn.addTarget(self, action: #selector(tvTapped), for: .touchDown)
        view.addSubview(ontvBtn)
        
        NSLayoutConstraint.activate([
            ontvBtn.leadingAnchor.constraint(equalTo: view.centerXAnchor),
            ontvBtn.centerYAnchor.constraint(equalTo: filterBg.centerYAnchor),
            ontvBtn.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: (screenWidth / 4) - 12)
        ])
        
        airBtn.setTitle("Airing Today", for: .normal)
        airBtn.titleLabel?.font = UIFont(name: "SFProText-Regular", size: 13)
        airBtn.setTitleColor(.white, for: .normal)
        airBtn.translatesAutoresizingMaskIntoConstraints = false
        airBtn.layer.cornerRadius = 8
        airBtn.addTarget(self, action: #selector(airTapped), for: .touchDown)
        view.addSubview(airBtn)
        
        NSLayoutConstraint.activate([
            airBtn.leadingAnchor.constraint(equalTo: ontvBtn.trailingAnchor),
            airBtn.trailingAnchor.constraint(equalTo: filterBg.trailingAnchor),
            airBtn.centerYAnchor.constraint(equalTo: filterBg.centerYAnchor)
        ])
    }
    
}
