//
//  ProfileViewController.swift
//  Yuni-Quintero-iOSChallenge
//
//  Created by Yuni Quintero on 2/2/21.
//

import UIKit

class ProfileViewController: UIViewController {
    
    let profilePic = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red:0.04, green:0.08, blue:0.11, alpha:1.0)
        
        let pageImg = UIImageView(image: UIImage(named: "pageImg"))
        pageImg.contentMode = .scaleAspectFit
        pageImg.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageImg)
        
        NSLayoutConstraint.activate([
            pageImg.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            pageImg.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        let titleLabel = UILabel()
        titleLabel.text = "Profile"
        titleLabel.font = UIFont(name: "SFProText-Bold", size: 24)
        titleLabel.textColor = UIColor(red:0.13, green:0.80, blue:0.40, alpha:1.0)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24),
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 40)
        ])
        
        profilePic.layer.masksToBounds = false
        profilePic.layer.cornerRadius = profilePic.frame.height/2 //PENDIENTE
        profilePic.clipsToBounds = true
        profilePic.image = UIImage(named: "userDefault")
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(profilePic)
        
        NSLayoutConstraint.activate([
            profilePic.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: -20),
            profilePic.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 50),
            profilePic.heightAnchor.constraint(equalToConstant: 125),
            profilePic.widthAnchor.constraint(equalToConstant: 125)
        ])
        
        let userTitle = UILabel()
        userTitle.text = "User Name" //PENDIENTE
        userTitle.numberOfLines = 5
        userTitle.textColor = .white
        userTitle.font = UIFont(name: "SFProText-Bold", size: 18)
        userTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(userTitle)
        
        NSLayoutConstraint.activate([
            userTitle.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: 10),
            userTitle.bottomAnchor.constraint(equalTo: profilePic.centerYAnchor)
        ])
        
        let userHandle = UILabel()
        userHandle.text = "@userhandle" //PENDIENTE
        userHandle.textColor = UIColor(red:0.13, green:0.80, blue:0.40, alpha:1.0)
        userHandle.font = UIFont(name: "SFProText-Bold", size: 12)
        userHandle.numberOfLines = 5
        userHandle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(userHandle)
        
        NSLayoutConstraint.activate([
            userHandle.topAnchor.constraint(equalTo: userTitle.bottomAnchor, constant: 15),
            userHandle.leadingAnchor.constraint(equalTo: userTitle.leadingAnchor)
        ])
        
        let favTitle = UILabel()
        favTitle.text = "Favorite Shows"
        favTitle.textColor = UIColor(red:0.13, green:0.80, blue:0.40, alpha:1.0)
        favTitle.font = UIFont(name: "SFProText-Bold", size: 18)
        favTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(favTitle)
        
        NSLayoutConstraint.activate([
            favTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            favTitle.topAnchor.constraint(equalTo: profilePic.bottomAnchor, constant: 50)
        ])
    }
}

